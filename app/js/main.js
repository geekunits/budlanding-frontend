$(function() {

	/****************
	** feedback owl *
	****************/

	$(".owl-feedback").owlCarousel({
		nav:true,
		dots:false,
		margin:30,
		items:1,
		responsive : {
			768 : {
				items: 1
			},

			992 : {
				items: 2
			},

			1200 : {
				items: 2
			}
		}
	});

	/******************
	** feedback audio *
	******************/

	$(".owl-feedback__audio-play").on("click", function(e){
		e.preventDefault();

		var $this = $(this);
		var a = $(this).find('audio')[0];

		a.onended = function() {
			$this.removeClass('playing');
		};

		if (a.paused || a.ended) {
			a.play();
			$this.addClass('playing');
		} else {
			$this.removeClass('playing');
			a.pause();
		};
	});

	/******************
	** main works sli *
	******************/

	var $finp = $(".works__ia-figure input");

	$finp.on('click', function(e){
		e.stopPropagation();
		$this = $(this);

		$finp.parent().removeClass('active');
		$this.parent().addClass('active');
	});

	$(".works__ia-prev").on("click", function(e){
		e.preventDefault();
		var id = $(".works__ia-figure.active").data('figure');
		
		if (id != 1) 
			$(".works__ia-figure.active").parent().children('input').click();
	});

	$(".works__ia-next").on("click", function(e){
		e.preventDefault();
		var id = $(".works__ia-figure.active").data('figure');

		if (id != 4)
			$(".works__ia-figure.active").children('.works__ia-figure').children('input').click();
	});

	function RecurringTimer(callback, delay) {
		var timerId, start, remaining = delay;

		this.pause = function() {
			window.clearTimeout(timerId);
			remaining -= new Date() - start;
		};

		var resume = function() {
			start = new Date();
			timerId = window.setTimeout(function() {
				remaining = delay;
				resume();
				callback();
			}, remaining);
		};

		this.resume = resume;

		this.resume();
	};

	function slideClick(){
		var $slide = $(".works__ia-figure.active");
		var id = $slide.data('figure');

		if (id != 4) {
			$(".works__ia-figure.active").children('.works__ia-figure').children('input').click();
		} else {
			$(".works__ia-container").children('.works__ia-figure').children('input').click();
		};
	};

	var mainSlider = new RecurringTimer(slideClick, 4000);

	$('.works__ia-container').hover(function() {
		mainSlider.pause();
	}, function() {
		mainSlider.resume();
	});

	/****
	* timer	
	****/

	$("#header-in-bot__timer").countdown({
		until: new Date(2017, 7-1, 14),
		layout: '{hnn}{sep}{mnn}{sep}{snn}',
		format: 'HMS'
	});

	/*************************
	** main works sli mobile *
	*************************/

	$(".xs-slider-block").on("click", function(e){
		var $this = $(this);

		if ( !$(".xs-slider-block").hasClass('active') ) {
			$this.addClass('active');
			$this.css("z-index", "10");
		} else if ( $this.hasClass('active') ) {
			$this.css("z-index", "10");
			setTimeout(function(){
				$this.css("z-index", "1");
			}, 750);
			$(".xs-slider-block").removeClass("active");
		};
	});

	/******************************
	** stepprice step 3 inp focus *
	******************************/

	$('.stepprice-label').on("click", function(e){
		var $this = $(this);

		if ( $this.find('.stepprice-input-metr-wrap').length ) {
			$this.find('.stepprice-input-metr-wrap').addClass('focused');
			$(".stepprice-radio").attr('checked', false);
		} else {
			$('.stepprice-input-metr-wrap').removeClass('focused');
		};
	});

	/******************************
	** sm xs menu *
	******************************/

	$('.menu-xs__dropdown-btn').on('click', function (e) {
		e.preventDefault();
		$(this).toggleClass('open');
		$(this).parents('.dropdown').find('.dropdown-menu').first().stop(true, true).fadeToggle(300);
	});

	$('body').on('click', function (e) {
		if ( !$('.dropdown').is(e.target) && $('.dropdown').has(e.target).length === 0 && $('.open').has(e.target).length === 0 ) {
			$('.dropdown').find('.dropdown-menu').first().stop(true, true).fadeOut(300);
			$('.dropdown').find('.menu-xs__dropdown-btn').removeClass('open');
		}
	});

	/*
	* scroll to
	*/

	$('.header-nav__link:not(.header-nav__link-callback)').on("click", function(e){
		e.preventDefault();
		var $this = $(this);
		var href = $this.attr("href");

		$("html, body").stop().animate({ scrollTop: $(href).offset().top }, 1000);
	});

	$('.menu-xs__link:not(.menu-xs__callback-btn)').on("click", function(e){
		e.preventDefault();
		var $this = $(this);
		var href = $this.attr("href");

		$('.dropdown').find('.dropdown-menu').first().stop(true, true).fadeOut(300);
		$('.dropdown').find('.menu-xs__dropdown-btn').removeClass('open');
		
		$("html, body").stop().animate({ scrollTop: $(href).offset().top }, 1000);
	});

	/*
	* fixed nav bar
	*/

	$(window).scroll(function() {
		var $nav = $('.header-nav');
		var $midElem = $('.header-in-top');
		var statickPosition = $midElem[0].offsetTop + $midElem.outerHeight();
		var winScroll = window.pageYOffset;
		var navHeight = $nav.outerHeight();
		var fromTop = $(this).scrollTop()+navHeight;

		var box = $midElem[0].getBoundingClientRect();
		var body = document.body;
		var docElem = document.documentElement;
		var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
		var clientTop = docElem.clientTop || body.clientTop || 0;
		var statickPosition = box.top +  scrollTop - clientTop + $midElem.outerHeight();

		if (winScroll > statickPosition) {
			$nav.addClass('fixed-nav-panel');
			$midElem.css('margin-bottom', navHeight+'px');
		} else {
			$nav.removeClass('fixed-nav-panel');
			$midElem.css('margin-bottom', '0');
		}                  
	});

}());